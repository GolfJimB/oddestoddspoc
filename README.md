# README #


### How do I get set up? ###
1. Update the connection strings in the web.config if necessary to point to your local sql server instance.
2. Run application once to trigger code-first database creation and population
3. In Global.asax un-comment SqlDepencyStart line 

To test the 'back-office' register any user

## Troubleshooting ##
If the changes aren't being fired or you get database related errors you may need to enable the broker with the below sql command

ALTER DATABASE OddestOddsPOC SET ENABLE_BROKER