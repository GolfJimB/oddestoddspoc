# ASSUMPTIONS #

As this is just a proof of concept, no optimisations have been made in regards to dealing with large amounts of data.
In production, the odds would be paged, and would be returned using IQueryable interface so as to allow EntityFramework to defer execution of any database queries so only the required amount of rows are returned.

In production the Entites would better normalised, i.e. seperate tables for teams.

Security considerations have been minimal in the creation of this proof of concept. 
Overposting and some basic input sanitastion has been implemented but in production much more would need to be considered, including: XSS prevention as we are updating the page contents with js this is a big threat, enumeration attacks when entites are referenced by id, proper sessions handling, ddos mitigation, error handling etc

Authentication is part of the proof of concept, however authorisation is not. In production the system could distingush between different levels of authorisation using the built .net roles etc.

Architectural considerations: a more extensible n-tier architecture would've been used in production including features like a service layer and sepearte projects for services, interfaces, models etc that would've taken advanted of the DI.
