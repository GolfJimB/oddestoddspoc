﻿
using System.Web;
using Microsoft.Web.WebSockets;
using OddestOddsPOC.Controllers;

namespace OddestOddsPOC
{
    public class WebSocketsServer : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            if (context.IsWebSocketRequest)
            {
                context.AcceptWebSocketRequest(new OddsWebSocketHandler());
            }
        }

        public bool IsReusable
        {
            get
            {
            return false;
            }
        }
    }
}