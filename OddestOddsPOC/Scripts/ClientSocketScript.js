﻿
function createSpan(text) {
    var span = document.createElement('span');
    span.innerHTML = text + '<br />';
    return span;
}

window.onload = function () {
    var conversation = $('#conversation');
    var url = 'ws://localhost:23133/WebSocketsServer.ashx?chatName=Jim';
    var ws = new WebSocket(url);

    ws.onerror = function (e) {
        conversation.appendChild(createSpan('Problem with connection: ' + e.message));
    };


    ws.onopen = function () {
        //the below can be uncommented for testing
        //conversation.innerHTML = 'Client connected <br/>';
    };

    ws.onmessage = function (e) {
        var odds = JSON.parse(e.data);
        //update every odd in the table
        odds.forEach(function (odd) {
            var el = document.querySelectorAll('tr[data-row-id="' + odd.Id + '"]')[0];
            //update existing rows
            if (el) {
                el.querySelector('td[data-role="homeodds"]').innerHTML = odd.HomeOdd;
                el.querySelector('td[data-role="awayodds"]').innerHTML = odd.AwayOdd;
                el.querySelector('td[data-role="drawodds"]').innerHTML = odd.DrawOdd;
                el.querySelector('td[data-role="timestamp"]').innerHTML = odd.DateTimeOfOdd;
            }
            //new row
            else {
                var tableRef = document.getElementById('oddsTable').getElementsByTagName('tbody')[0];
                var row = tableRef.insertRow(tableRef.rows.length);
                $(row).attr('data-row-id', odd.Id)
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);
                var cell6 = row.insertCell(5);
                var cell7 = row.insertCell(6);
                var cell8 = row.insertCell(7);

                cell1.innerHTML = odd.LeagueName
                cell2.innerHTML = odd.HomeTeamName;
                cell3.innerHTML = odd.AwayTeamName;
                cell4.innerHTML = odd.DateTimeOfOdd;
                cell5.innerHTML = odd.HomeOdd;
                cell6.innerHTML = odd.AwayOdd;
                cell7.innerHTML = odd.DrawOdd;
                cell8.innerHTML = "<a href='/Home/Edit/" + odd.Id + "'>Edit</a> | <a href='/Home/Delete/" + odd.Id + "'>Delete</a>";
            }
        });

        var rows = $('tr[data-row-id]');

        $.each(rows, function () {
            var id = parseInt($(this).attr('data-row-id'), 10);

            var odd = odds.filter(function (x) { return x.Id === id });
            //if any rows in the table don't have a corresponding id in the odds returned from the server then the row is deleted.
            if (odd.length === 0) {
                $(this).remove();
            }
        });

        //conversation.appendChild(createSpan(e.data.toString()));
    };

    ws.onclose = function () {
        conversation.innerHTML = 'Closed connection!';
    };

};