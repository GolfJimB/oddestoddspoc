﻿using OddestOddsPOC.Models.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace OddestOddsPOC.DAL
{
    public class OddestOddsContext : DbContext
    {

        public OddestOddsContext() : base("OddestOddsContext")
        {
        }

        public DbSet<Odd> Odds { get; set; }
        public DbSet<League> Leagues { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
