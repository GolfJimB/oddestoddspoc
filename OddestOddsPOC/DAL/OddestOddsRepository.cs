﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using OddestOddsPOC.Models.Entities;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity;

namespace OddestOddsPOC.DAL
{
    public class OddestOddsRepository : IOddestOddsRepository
    {
        private OddestOddsContext db = new OddestOddsContext();

        public void CreateOdd(Odd odd)
        {
            db.Odds.Add(odd);
            db.SaveChanges();
        }

        public void DeleteOdd(int id)
        {
            var odd = db.Odds.Find(id);
            db.Odds.Remove(odd);
            db.SaveChanges();
        }

        public void EditOdd(Odd odd)
        {
            db.Entry(odd).State = EntityState.Modified;
            db.SaveChanges();
        }

        public IQueryable<Odd> GetAllOdds()
        {
            return db.Odds;
        }

        public Odd GetOddById(int? id)
        {
            return db.Odds.FirstOrDefault(o => o.Id == id);
        }
    }
}