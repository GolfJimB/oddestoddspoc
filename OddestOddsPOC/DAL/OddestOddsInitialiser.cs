﻿using OddestOddsPOC.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddestOddsPOC.DAL
{
    public class OddestOddsInitialiser : System.Data.Entity.DropCreateDatabaseIfModelChanges<OddestOddsContext>
    {
        protected override void Seed(OddestOddsContext context)
        {
            var leagues = new List<League>
            {
                new League {LeagueName="Premier League" },
                new League {LeagueName = "Bundesliga" }
            };
            leagues.ForEach(l => context.Leagues.Add(l));
            context.SaveChanges();

            var odds = new List<Odd>
            {
                new Odd { LeagueId = 1, HomeTeamName = "Arsenal", AwayTeamName = "West Bromwich", HomeOdd = 15.0m, DrawOdd = 6.50m, AwayOdd = 1.30m, DateTimeOfOdd = DateTime.Now },
                new Odd { LeagueId = 1, HomeTeamName = "Everton", AwayTeamName = "Southampton", HomeOdd = 2.72m, DrawOdd = 3.40m,  AwayOdd = 2.90m, DateTimeOfOdd = DateTime.Now },
                new Odd { LeagueId = 1, HomeTeamName = "Manchester Utd.", AwayTeamName = "Aston Villa", HomeOdd = 1.26m, DrawOdd = 7.00m,  AwayOdd = 17.00m, DateTimeOfOdd = DateTime.Now },
                new Odd { LeagueId = 1, HomeTeamName = "Newcastle", AwayTeamName = "Swansea", HomeOdd = 2.38m, DrawOdd = 3.50m,  AwayOdd = 3.40m, DateTimeOfOdd = DateTime.Now },
                new Odd { LeagueId = 2, HomeTeamName = "Hannover", AwayTeamName = "B.M'gladbach", HomeOdd = 5.50m, DrawOdd = 4.33m,  AwayOdd = 1.70m, DateTimeOfOdd = DateTime.Now }
            };
            odds.ForEach(o => context.Odds.Add(o));
            context.SaveChanges();
            
        }
    }
}