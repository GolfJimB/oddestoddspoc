﻿using OddestOddsPOC.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OddestOddsPOC.DAL
{
    public interface IOddestOddsRepository
    {
        void CreateOdd(Odd odd);
        void EditOdd(Odd odd);
        IQueryable<Odd> GetAllOdds();
        Odd GetOddById(int? id);
        void DeleteOdd(int id);
    }
}
