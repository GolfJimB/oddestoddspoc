﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using OddestOddsPOC.DAL;
using OddestOddsPOC.Models.Entities;
using OddestOddsPOC.Models.ViewModels;

namespace OddestOddsPOC.Controllers
{
    public class HomeController : Controller
    {
        private IOddestOddsRepository db;
        public HomeController(IOddestOddsRepository repo)
        {
            db = repo;
        }
        // GET: Odds
        public ActionResult Index()
        {
            return View(new HomePageViewModel(db.GetAllOdds().ToList()));
        }

        // GET: Odds/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Odds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LeagueId,HomeTeamName,AwayTeamName,HomeOdd,AwayOdd,DrawOdd")] Odd odd)
        {
            odd.DateTimeOfOdd = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.CreateOdd(odd);
                return RedirectToAction("Index");
            }

            return View(odd);
        }

        // GET: Odds/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Odd odd = db.GetOddById(id);
            if (odd == null)
            {
                return HttpNotFound();
            }
            return View(odd);
        }

        // POST: Odds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LeagueId,HomeOdd,AwayOdd,DrawOdd")] Odd odd)
        {
            var existing = db.GetOddById(odd.Id);
            if (existing != null)
            {
                existing.HomeOdd = odd.HomeOdd;
                existing.DrawOdd = odd.DrawOdd;
                existing.AwayOdd = odd.AwayOdd;
                existing.DateTimeOfOdd = DateTime.Now;
                if (ModelState.IsValid)
                {
                    db.EditOdd(existing);
                    return RedirectToAction("Index");
                }
            }
            return View(odd);
        }

        // GET: Odds/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Odd odd = db.GetOddById(id);
            if (odd == null)
            {
                return HttpNotFound();
            }
            return View(odd);
        }

        // POST: Odds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            db.DeleteOdd(id);
            return RedirectToAction("Index");
        }        
    }
}
