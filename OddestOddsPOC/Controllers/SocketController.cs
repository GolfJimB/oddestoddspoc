﻿using Microsoft.Web.WebSockets;
using OddestOddsPOC.DAL;
using OddestOddsPOC.Models.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace OddestOddsPOC.Controllers
{
    public class OddsWebSocketHandler : WebSocketHandler
    {
        private static WebSocketCollection clients = new WebSocketCollection();
        private string name;

        public OddsWebSocketHandler()
        {
            SetupNotifier();
        }

        protected void SetupNotifier()
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["OddestOddsContext"].ConnectionString))
            {
                connection.Open();
                //The command for which any changes to the result will fire the onChangeEventHandler
                using (SqlCommand command = new SqlCommand(@"SELECT [Id] ,[LeagueId] ,[HomeTeamName] ,[AwayTeamName] ,[DateTimeOfOdd] ,[HomeOdd] ,[AwayOdd] ,[DrawOdd] FROM [OddestOddsPOC].[dbo].[Odd]", connection))
                {
                    command.Notification = null;
                    SqlDependency dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }
                    var reader = command.ExecuteReader();

                    reader.Close();
                }
            }
        }

        public override void OnOpen()
        {
            //Add client to this list of clients so they can all be updated
            clients.Add(this);
        }

        public override void OnMessage(string message)
        {
        }

        public override void OnClose()
        {
            //remove client
            clients.Remove(this);
        }

        //triggered when there's a change in the odds table in the database
        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            using (var db = new OddestOddsContext())
            {
                var odds = db.Odds.Select(x => new { x.AwayOdd, x.AwayTeamName, x.DateTimeOfOdd, x.DrawOdd, x.HomeOdd, x.HomeTeamName, x.Id, x.LeagueId, x.League.LeagueName}).ToList();
                clients.Broadcast(Newtonsoft.Json.JsonConvert.SerializeObject(odds));
            }
        }
    }
}
