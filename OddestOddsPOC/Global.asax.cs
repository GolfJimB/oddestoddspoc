﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;
using System.Data.SqlClient;
using System.Configuration;

namespace OddestOddsPOC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            //SqlDependency.Start(ConfigurationManager.ConnectionStrings["OddestOddsContext"].ConnectionString);
        }
        void Application_End()
        {
            SqlDependency.Stop(ConfigurationManager.ConnectionStrings["OddestOddsContext"].ConnectionString);
        }
    }
}
