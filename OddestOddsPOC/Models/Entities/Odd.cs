﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace OddestOddsPOC.Models.Entities
{
    public class Odd
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Display(Name = "League")]
        [Range(1, 2)]
        public int LeagueId { get; set; }
        [Display(Name = "Home")]
        public string HomeTeamName { get; set; }
        [Display(Name = "Away")]
        public string AwayTeamName { get; set; }
        [Display(Name = "Timestamp")]
        [DataType(DataType.DateTime)]
        public DateTime DateTimeOfOdd { get; set; }
        [Display(Name = "HW")]
        public decimal HomeOdd { get; set; }
        [Display(Name = "AW")]
        public decimal AwayOdd { get; set; }
        [Display(Name = "D")]
        public decimal DrawOdd { get; set; }

        public virtual League League { get; set; }
    }
}