﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OddestOddsPOC.Models.ViewModels
{
    public class HomePageViewModel
    {
        public IEnumerable<Entities.Odd> Odds { get; set; }
        public LoginViewModel Login { get; set; }
        public HomePageViewModel(IEnumerable<Entities.Odd> odds)
        {
            Odds = odds;
            Login = new LoginViewModel();
        }
    }
}