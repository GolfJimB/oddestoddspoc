﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OddestOddsPOC.Startup))]
namespace OddestOddsPOC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
